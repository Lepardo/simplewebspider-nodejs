module.exports = {
  preset: `ts-jest`,
  testEnvironment: `node`,
  coverageDirectory: `dist-coverage`,
  collectCoverage: true,
  collectCoverageFrom: [`src/**/*.ts`],
  reporters: [
    `default`,
    [
      `jest-junit`,
      {
        outputDirectory: `dist-coverage`,
        outputName: `junit.xml`,
      },
    ],
  ],
};
