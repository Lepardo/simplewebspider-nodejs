import { validateSync } from "class-validator";
import { ValidationCheckError } from "./ValidationCheckError";

// eslint-disable-next-line @typescript-eslint/ban-types
export function checkValidateSync(o: Object): void {
  const errors = validateSync(o);
  if (errors?.length > 0) {
    throw new ValidationCheckError(`Validation failed`, errors);
  }
}
