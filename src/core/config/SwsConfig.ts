import { IsDefined } from "class-validator";
import { BootstrapConfig, IBootstrapConfig } from "../../service/bootstrap";
import { DatabaseConfig, HostThrottlerConfig, IDatabaseConfig, IHostThrottlerConfig } from "../../service/database";
import { ExtractorConfig, FilterInvalidUrlConfig, IExtractorConfig, IFilterInvalidUrlConfig } from "../../service/extractor";
import { IQueueConfig, IQueueThrottlerConfig, QueueConfig, QueueThrottlerConfig } from "../../service/queue";
import { ILogConfig, LogConfig } from "../logger";
import { checkValidateSync } from "../utils";
import { BuildConfig } from "./BuildConfig";
import { IBuildConfig } from "./IBuildConfig";
import { ISwsConfig } from "./ISwsConfig";

export class SwsConfig implements ISwsConfig {
  @IsDefined()
  log: ILogConfig;
  @IsDefined()
  bootstrap: IBootstrapConfig;
  @IsDefined()
  build: IBuildConfig;
  @IsDefined()
  database: IDatabaseConfig;
  @IsDefined()
  queue: IQueueConfig;
  @IsDefined()
  queueThrottler: IQueueThrottlerConfig;
  @IsDefined()
  hostThrottler: IHostThrottlerConfig;
  @IsDefined()
  extractor: IExtractorConfig;
  @IsDefined()
  filterInvalidUrl: IFilterInvalidUrlConfig;

  constructor(that: ISwsConfig) {
    this.log = new LogConfig(that.log);
    this.bootstrap = new BootstrapConfig(that.bootstrap);
    this.build = new BuildConfig(that.build);
    this.database = new DatabaseConfig(that.database);
    this.queue = new QueueConfig(that.queue);
    this.queueThrottler = new QueueThrottlerConfig(that.queueThrottler);
    this.hostThrottler = new HostThrottlerConfig(that.hostThrottler);
    this.extractor = new ExtractorConfig(that.extractor);
    this.filterInvalidUrl = new FilterInvalidUrlConfig(that.filterInvalidUrl);
    checkValidateSync(this);
  }
}
