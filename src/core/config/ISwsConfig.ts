import { IBootstrapConfig } from "../../service/bootstrap";
import { IDatabaseConfig, IHostThrottlerConfig } from "../../service/database";
import { IExtractorConfig, IFilterInvalidUrlConfig } from "../../service/extractor";
import { IQueueConfig, IQueueThrottlerConfig } from "../../service/queue";
import { ILogConfig } from "../logger";
import { IBuildConfig } from "./IBuildConfig";

export interface ISwsConfig {
  readonly log: ILogConfig;
  readonly bootstrap: IBootstrapConfig;
  readonly build: IBuildConfig;
  readonly database: IDatabaseConfig;
  readonly queue: IQueueConfig;
  readonly queueThrottler: IQueueThrottlerConfig;
  readonly hostThrottler: IHostThrottlerConfig;
  readonly extractor: IExtractorConfig;
  readonly filterInvalidUrl: IFilterInvalidUrlConfig;
}
