import Errlop from "errlop";

export class EnvFileMissingError extends Errlop {
  constructor(message: string) {
    super(message);
    this.name = `EnvFileMissingError`;
  }
}
