import Errlop from "errlop";

export class InvalidConfigurationError extends Errlop {
  constructor(message: string, e?: Error) {
    super(message, e);
    this.name = `InvalidConfigurationError`;
  }
}
