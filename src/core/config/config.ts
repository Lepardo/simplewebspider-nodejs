/* eslint-disable @typescript-eslint/no-non-null-assertion */
import dotenv from "dotenv";
import fs from "fs-extra";
import { envOverride } from "../../resources";
import { toLogLevel } from "../logger";
import { EnvFileMissingError } from "./EnvFileMissingError";
import { InvalidConfigurationError } from "./InvalidConfigurationError";
import { SwsConfig } from "./SwsConfig";

function loadConfig(filename: string): void {
  if (!fs.existsSync(filename)) {
    throw new EnvFileMissingError(`File does not exists [filename=${filename}]`);
  }

  const envConfig = dotenv.parse(fs.readFileSync(filename));
  for (const k in envConfig) {
    process.env[k] = envConfig[k];
  }
}

function initializeConfiguration(): SwsConfig {
  try {
    dotenv.config();
    loadConfig(envOverride);

    return new SwsConfig({
      build: {
        name: process.env.BUILD_NAME!,
        version: process.env.BUILD_VERSION!,
        timestamp: process.env.BUILD_TIMESTAMP!,
        sha: process.env.BUILD_SHA!,
      },
      log: {
        level: toLogLevel(process.env.LOG_LEVEL),
      },
      bootstrap: {
        filename: process.env.BOOTSTRAP_FILE || `bootstrap.txt`,
      },
      database: {
        size: Number(process.env.DATABASE_SIZE) || 10 * 1024 * 1024,
      },
      queue: {
        parallel: Number(process.env.QUEUE_PARALLEL) || 4,
      },
      queueThrottler: {
        perMinute: Number(process.env.QUEUE_THROTTLE_PER_MINUTE) || 10,
        minWait: Number(process.env.QUEUE_THROTTLE_MIN_WAIT) || 100,
        randomWait: Number(process.env.QUEUE_THROTTLE_RANDOM_WAIT) || 1000,
      },
      hostThrottler: {
        size: Number(process.env.HOST_THROTTLE_SIZE) || 1024 * 1024,
        preload: Number(process.env.HOST_THROTTLE_PRELOAD) || 128,
        ttl: Number(process.env.HOST_THROTTLE_TTL) || 60 * 60 * 1000,
      },
      extractor: {
        httpMaxResponseSize: Number(process.env.EXTRACTOR_HTTP_MAX_RESPONSE_SIZE) || 10 * 1024 * 1024,
        httpTimeout: Number(process.env.EXTRACTOR_HTTP_TIMEOUT) || 30 * 1000,
        urlMaxLength: Number(process.env.EXTRACTOR_URL_MAX_LENGTH) || 1024,
      },
      filterInvalidUrl: {
        allowedSize: Number(process.env.FILTER_INVALID_URL_ALLOWED_SIZE) || 1024 * 1024,
        blockedSize: Number(process.env.FILTER_INVALID_URL_BLOCKED_SIZE) || 1024 * 1024,
      },
    });
  } catch (e) {
    throw new InvalidConfigurationError(`Configuration invalid`, e);
  }
}

const config = initializeConfiguration();

export { config };
