import { IsInt, IsPositive, Max, Min } from "class-validator";
import { checkValidateSync } from "../../core/utils";
import { IHostThrottlerConfig } from "./IHostThrottlerConfig";

export class HostThrottlerConfig implements IHostThrottlerConfig {
  @IsInt()
  @IsPositive()
  @Max(Number.MAX_SAFE_INTEGER)
  preload: number;
  @IsInt()
  @IsPositive()
  @Max(Number.MAX_SAFE_INTEGER)
  size: number;
  @IsInt()
  @Min(0)
  @Max(Number.MAX_SAFE_INTEGER)
  ttl: number;

  constructor(that: IHostThrottlerConfig) {
    this.size = that.size;
    this.preload = that.preload;
    this.ttl = that.ttl;
    checkValidateSync(this);
  }
}
