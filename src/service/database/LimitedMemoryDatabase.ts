import { IsDefined, IsInt, IsPositive } from "class-validator";
import { logger } from "../../core/logger";
import { checkValidateSync, randomInt } from "../../core/utils";
import { IDatabase } from "./IDatabase";
import { IDatabaseConfig } from "./IDatabaseConfig";

class LimitedMemoryDatabase implements IDatabase {
  @IsPositive()
  @IsInt()
  readonly maxSize: number;

  @IsDefined()
  entries: Set<string>;

  constructor(databaseConfig: IDatabaseConfig) {
    this.maxSize = databaseConfig.size;
    this.entries = new Set();
    checkValidateSync(this);
  }

  async size(): Promise<number> {
    return this.entries.size;
  }

  async remove(): Promise<URL | undefined> {
    const index = randomInt(0, this.entries.size);
    logger.silly(`Removing element [index=${index}, length=${this.entries.size}]`);
    let count = 0;
    let item: URL | undefined = undefined;
    this.entries.forEach((value) => {
      if (count++ === index) {
        item = new URL(value);
      }
    });
    if (item) {
      this.entries.delete(item);
    }
    return item;
  }

  async add(...items: URL[]): Promise<this> {
    logger.debug(`Add entry: ${items}`);
    items.forEach((item) => this.entries.add(item.toString()));
    while (this.entries.size > this.maxSize) {
      this.entries.forEach((value, _value, set) => {
        if (set.size > this.maxSize) {
          logger.silly(`Deleting entry [size=${set.size}, value=${value}]`);
          set.delete(value);
        }
      });
    }
    return this;
  }
}

export { LimitedMemoryDatabase };
