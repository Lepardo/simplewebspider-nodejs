export interface IDatabase {
  remove(): Promise<URL | undefined>;
  add(...items: URL[]): Promise<this>;
  size(): Promise<number>;
}
