import { IsDefined, IsInt, IsNotEmpty, IsPositive, Max, Min, NotContains } from "class-validator";
import Errlop from "errlop";
import { logger } from "../../core/logger";
import { checkValidateSync } from "../../core/utils";
import { IDatabase } from "./IDatabase";
import { IHostThrottlerConfig } from "./IHostThrottlerConfig";

class HostEntry {
  @IsNotEmpty()
  @NotContains(`:`)
  @NotContains(`/`)
  readonly hostname: string;
  @IsInt()
  @Min(0)
  @Max(Number.MAX_SAFE_INTEGER)
  usage: number;
  @IsInt()
  @Min(Number.MIN_SAFE_INTEGER)
  @Max(Number.MAX_SAFE_INTEGER)
  lastAccessEpoch: number;

  touch(): void {
    this.usage++;
    this.lastAccessEpoch = Date.now();
    logger.silly(`Touched host entry ${JSON.stringify(this)}`);
  }

  constructor(hostname: string) {
    this.hostname = hostname;
    this.lastAccessEpoch = Date.now();
    this.usage = 0;
    checkValidateSync(this);
  }
}

class HostStorage {
  @IsInt()
  @IsPositive()
  @Max(Number.MAX_SAFE_INTEGER)
  readonly size: number;
  @IsInt()
  @Min(0)
  @Max(Number.MAX_SAFE_INTEGER)
  readonly ttl: number;
  @IsDefined()
  hosts: HostEntry[];

  hostEntry(url: URL): HostEntry {
    this.removeOld();

    const hostname = url.hostname;
    if (!hostname) {
      throw new Errlop(`Invalid URL: no hostname [url=${url}]`);
    }

    let host = this.hosts.find((host) => host.hostname === hostname);
    if (host === undefined) {
      host = new HostEntry(hostname);
    }
    return host;
  }

  touch(url: URL): void {
    const hostEntry = this.hostEntry(url);
    if (hostEntry.usage === 0) {
      this.add(hostEntry);
    }
    hostEntry.touch();
  }

  removeOld(): void {
    const oldest = Date.now() - this.ttl;
    this.hosts = this.hosts.filter((entry) => entry.lastAccessEpoch > oldest);
  }

  add(hostEntry: HostEntry): void {
    this.hosts.push(hostEntry);
    this.hosts.sort((a, b) => b.lastAccessEpoch - a.lastAccessEpoch);
    this.hosts.splice(this.size, this.hosts.length - this.size);
  }

  constructor(config: IHostThrottlerConfig) {
    this.size = config.size;
    this.ttl = config.ttl;
    this.hosts = [];
    checkValidateSync(this);
  }
}

export class HostThrottlerDatabase implements IDatabase {
  @IsInt()
  @IsPositive()
  @Max(Number.MAX_SAFE_INTEGER)
  readonly preload: number;

  @IsDefined()
  readonly hostStorage: HostStorage;

  @IsDefined()
  readonly database: IDatabase;

  constructor(database: IDatabase, config: IHostThrottlerConfig) {
    this.database = database;
    this.preload = config.preload;
    this.hostStorage = new HostStorage(config);
    checkValidateSync(this);
  }

  async remove(): Promise<URL | undefined> {
    const candidates = await this.candidates();

    candidates.sort((a, b) => this.usageComperator(a, b));
    const item = candidates.shift();
    if (item === undefined) {
      return undefined;
    }

    await this.database.add(...candidates);

    try {
      this.hostStorage.touch(item);
    } catch (e) {
      throw new Errlop(`Failed to touch host entry [item=${item}]`, e);
    }
    return item;
  }

  async add(...items: URL[]): Promise<this> {
    await this.database.add(...items);
    return this;
  }

  async candidates(): Promise<URL[]> {
    const candidates: URL[] = [];
    while (candidates.length < this.preload) {
      const candidate = await this.database.remove();
      if (candidate === undefined) {
        break;
      }
      candidates.push(candidate);
    }
    return candidates;
  }

  usageComperator(a: URL, b: URL): number {
    const aEntry = this.hostStorage.hostEntry(a);
    const bEntry = this.hostStorage.hostEntry(b);
    return aEntry.usage - bEntry.usage;
  }

  async size(): Promise<number> {
    return await this.database.size();
  }
}
