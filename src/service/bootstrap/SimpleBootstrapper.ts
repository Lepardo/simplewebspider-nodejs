import { IsDefined } from "class-validator";
import Errlop from "errlop";
import fs from "fs-extra";
import { logger } from "../../core/logger";
import { checkValidateSync } from "../../core/utils";
import { bootstrapTxt } from "../../resources";
import { IDatabase } from "../database";
import { BootstrapError } from "./BootstrapError";
import { IBootstrapConfig } from "./IBootstrapConfig";
import { IBootstrapper } from "./IBootstrapper";

export class SimpleBootstrapper implements IBootstrapper {
  @IsDefined()
  readonly config: IBootstrapConfig;

  async run(database: IDatabase): Promise<void> {
    try {
      let filename = this.config.filename;
      const filenameExists = await fs.pathExists(filename);
      if (!filenameExists) {
        logger.info(`Bootstrap file not found: using embedded bootstrap file [filename=${filename}]`);
        filename = bootstrapTxt;
      }
      const content = await fs.readFile(filename, `utf8`);
      content
        .split(/\r?\n/) // Split by line break
        .map((line) => line && line.trim()) // Trim whitespaces
        .filter((line) => !!line) // Ignore empty lines
        .filter((line) => {
          // Skip comments
          const result = !line.startsWith(`#`) && !line.startsWith(`//`);
          if (!result) {
            logger.debug(`Skipping line: outcommented [line=${line}]`);
          }
          return result;
        }) //
        .filter((line) => {
          // Only allow http(s)
          const result = line.startsWith(`http://`) || line.startsWith(`https://`);
          if (!result) {
            logger.info(`Skipping line: not http or https [line=${line}]`);
          }
          return result;
        }) //
        .filter((line) => {
          // Only allow valid URLs
          try {
            new URL(line);
            return true;
          } catch (e) {
            logger.info(`Skipping line: no valid URL [line=${line}, error=${e?.message}]`);
            logger.debug(`Skipping line: no valid URL [line=${line}]`, e);
            return false;
          }
        }) //
        .map((line) => new URL(line)) //
        .forEach((url) => {
          logger.info(`Using URL [url=${url.toString()}]`);
          database.add(url);
        });

      const databaseSize = await database.size();
      if (databaseSize === 0) {
        throw new Errlop(`Bootstrapping failed: effectivly no URLs found`);
      }
    } catch (e) {
      throw new BootstrapError(`Bootstrapping failed [file=${this.config.filename}`, e);
    }
  }

  constructor(config: IBootstrapConfig) {
    this.config = config;
    checkValidateSync(this);
  }
}
