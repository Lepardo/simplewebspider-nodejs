import Errlop from "errlop";

export class BootstrapError extends Errlop {
  constructor(message: string, cause?: Error) {
    super(message, cause);
  }
}
