export interface IFilterInvalidUrlConfig {
  readonly allowedSize: number;
  readonly blockedSize: number;
}
