import { IsInt, Max, Min } from "class-validator";
import { checkValidateSync } from "../../core/utils";
import { IFilterInvalidUrlConfig } from "./IFilterInvalidUrlConfig";

export class FilterInvalidUrlConfig implements IFilterInvalidUrlConfig {
  @IsInt()
  @Min(0)
  @Max(Number.MAX_SAFE_INTEGER)
  allowedSize: number;
  @IsInt()
  @Min(0)
  @Max(Number.MAX_SAFE_INTEGER)
  blockedSize: number;

  constructor(that: IFilterInvalidUrlConfig) {
    this.allowedSize = that.allowedSize;
    this.blockedSize = that.blockedSize;
    checkValidateSync(this);
  }
}
