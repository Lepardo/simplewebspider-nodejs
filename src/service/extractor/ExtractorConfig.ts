import { Max, Min, IsInt } from "class-validator";
import { checkValidateSync } from "../../core/utils";
import { IExtractorConfig } from "./IExtractorConfig";

export class ExtractorConfig implements IExtractorConfig {
  @IsInt()
  @Min(0)
  @Max(Number.MAX_SAFE_INTEGER)
  httpMaxResponseSize: number;
  @IsInt()
  @Min(0)
  @Max(Number.MAX_SAFE_INTEGER)
  httpTimeout: number;
  @IsInt()
  @Min(0)
  @Max(Number.MAX_SAFE_INTEGER)
  urlMaxLength: number;

  constructor(that: IExtractorConfig) {
    this.httpMaxResponseSize = that.httpMaxResponseSize;
    this.httpTimeout = that.httpTimeout;
    this.urlMaxLength = that.urlMaxLength;
    checkValidateSync(this);
  }
}
