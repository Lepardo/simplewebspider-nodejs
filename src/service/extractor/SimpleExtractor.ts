import { IsInt, Max, Min } from "class-validator";
import { Handler } from "htmlparser2/lib/Parser";
import { WritableStream } from "htmlparser2/lib/WritableStream";
import fetch from "node-fetch";
import stream from "stream";
import { URL } from "url";
import util from "util";
import { logger } from "../../core/logger";
import { checkValidateSync } from "../../core/utils";
import { IExtractor } from "./IExtractor";
import { IExtractorConfig } from "./IExtractorConfig";

const pPipeline = util.promisify(stream.pipeline);

function isAbsolut(url: string): boolean {
  if (/^[a-zA-Z]:\\/.test(url)) {
    return false;
  }

  return /^[a-zA-Z][a-zA-Z\d+\-.]*:/.test(url);
}

function expandUrl(url: string, baseUrl: string): string {
  if (isAbsolut(url)) {
    return url;
  }

  const newUrl = new URL(url, baseUrl);
  return newUrl.toString();
}

function isValidUrl(urlString: string): boolean {
  try {
    new URL(urlString);
    return true;
  } catch (e) {
    if (e instanceof TypeError) {
      logger.debug(`Ignoring invalid URL [url=${urlString}, errorMessage=${e.message}]`, e);
      logger.info(`Ignoring invalid URL [url=${urlString}, errorMessage=${e.message}]`);
    } else {
      logger.info(`Ignoring invalid URL [url=${urlString}, errorMessage=${e.message}]`, e);
    }
  }
  return false;
}

function createUrlHandler(urls: Set<string>, url: string, urlMaxLength: number): Partial<Handler> {
  return {
    onopentag(
      name: string,
      attribs: {
        [s: string]: string;
      },
    ): void {
      logger.silly(`OpenTag: ${name} - ${JSON.stringify(attribs)}`);
      let newUrl: string | undefined = undefined;
      // TODO Implement RSS and ATOM Feed support
      if (name === `a` && attribs && attribs[`href`]) {
        newUrl = attribs[`href`];
      } else if (name === `img` && attribs && attribs[`src`]) {
        newUrl = attribs[`src`];
      }

      if (newUrl) {
        newUrl = expandUrl(newUrl, url);
        if (isAbsolut(newUrl) && newUrl.length <= urlMaxLength) {
          urls.add(newUrl);
        }
      }
    },
  };
}

export class SimpleExtractor implements IExtractor {
  @IsInt()
  @Min(0)
  @Max(Number.MAX_SAFE_INTEGER)
  readonly httpMaxSize: number;
  @IsInt()
  @Min(0)
  @Max(Number.MAX_SAFE_INTEGER)
  readonly httpTimeout: number;
  @IsInt()
  @Min(0)
  @Max(Number.MAX_SAFE_INTEGER)
  readonly urlMaxLength: number;

  async extract(url: URL): Promise<Set<URL>> {
    const urlStrings: Set<string> = new Set();
    try {
      const response = await fetch(url, {
        size: this.httpMaxSize,
        timeout: this.httpTimeout,
      });
      if (!response.ok) {
        logger.debug(`Request failed [url = ${url}, status = ${response.status}, statusText = ${response.statusText}]`);
        return new Set();
      }

      // TODO Check response type. We can't process binaries only html

      const parserStream = new WritableStream(createUrlHandler(urlStrings, url.toString(), this.urlMaxLength));

      await pPipeline(response.body, parserStream);
    } catch (e) {
      logger.warn(`Failed to extract [url = ${url}]`, e);
    }

    const urls = [...urlStrings] //
      .filter((u) => isValidUrl(u)) //
      .map((u) => new URL(u));

    const result = new Set(urls);
    return result;
  }
  constructor(config: IExtractorConfig) {
    this.httpMaxSize = config.httpMaxResponseSize;
    this.httpTimeout = config.httpTimeout;
    this.urlMaxLength = config.urlMaxLength;
    checkValidateSync(this);
  }
}
