import { IsDefined, IsInt, Max, Min } from "class-validator";
import { LookupAddress, promises as dns } from "dns";
import { filter } from "p-iteration";
import isPrivateIp from "private-ip";
import { logger } from "../../core/logger";
import { checkValidateSync } from "../../core/utils";
import { IExtractor } from "./IExtractor";
import { IFilterInvalidUrlConfig } from "./IFilterInvalidUrlConfig";

function reunshift<T>(array: T[], entry: T): T[] {
  array = array.filter((item) => item !== entry);
  array.unshift(entry);
  return array;
}

function unshift<T>(array: T[], entry: T, limit: number): T[] {
  array.unshift(entry);
  if (limit > 0) {
    array.splice(limit, array.length - limit);
  }
  return array;
}

export class FilterInvalidUrlsExtrator implements IExtractor {
  @IsDefined()
  readonly extractor: IExtractor;
  @IsDefined()
  blockedHostnames: string[];
  @IsDefined()
  allowedHostnames: string[];
  @IsInt()
  @Min(0)
  @Max(Number.MAX_SAFE_INTEGER)
  readonly blockedSize: number;
  @IsInt()
  @Min(0)
  @Max(Number.MAX_SAFE_INTEGER)
  readonly allowedSize: number;

  async extract(url: URL): Promise<Set<URL>> {
    let urls = [...(await this.extractor.extract(url))];
    urls = urls.filter((url) => this.isHttp(url));
    urls = await filter(urls, async (url) => await this.isAllowed(url));
    return new Set(urls);
  }

  isHttp(url: URL): boolean {
    return url.toString().startsWith(`http://`) || url.toString().startsWith(`https://`);
  }

  async isAllowed(url: URL): Promise<boolean> {
    const hostname = url.hostname;

    const allowed = this.allowedHostnames.find((a) => a === hostname);
    if (allowed) {
      logger.silly(`Already allowed [hostname=${hostname}]`);
      this.allowedHostnames = reunshift(this.allowedHostnames, hostname);
      return true;
    }

    const blocked = this.blockedHostnames.find((b) => b === hostname);
    if (blocked) {
      logger.silly(`Already blocked [hostname=${hostname}]`);
      this.blockedHostnames = reunshift(this.blockedHostnames, hostname);
      return false;
    }

    let lookup: LookupAddress;
    try {
      lookup = await dns.lookup(hostname);
    } catch (e) {
      logger.info(`Failed to lookup dns. Add to block list [hostename=${hostname}, error=${e?.message}]`);
      logger.debug(`Failed to lookup dns. Add to block list [hostename=${hostname}]`, e);
      unshift(this.blockedHostnames, hostname, this.blockedSize);
      return false;
    }

    const address = lookup.address;
    const privateIp = isPrivateIp(address);
    if (privateIp) {
      logger.info(`Blocking [hostname=${hostname}]`);
      unshift(this.blockedHostnames, hostname, this.blockedSize);
      return false;
    }

    logger.debug(`Allowing [hostname=${hostname}]`);
    unshift(this.allowedHostnames, hostname, this.allowedSize);
    return true;
  }

  constructor(extractor: IExtractor, config: IFilterInvalidUrlConfig) {
    this.extractor = extractor;
    this.blockedSize = config.allowedSize;
    this.allowedSize = config.blockedSize;
    this.blockedHostnames = [];
    this.allowedHostnames = [];
    checkValidateSync(this);
  }
}
