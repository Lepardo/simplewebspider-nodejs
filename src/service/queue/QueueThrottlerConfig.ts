import { IsInt, Max, Min } from "class-validator";
import { checkValidateSync } from "../../core/utils";
import { IQueueThrottlerConfig } from "./IQueueThrottlerConfig";

export class QueueThrottlerConfig implements IQueueThrottlerConfig {
  @IsInt()
  @Min(0)
  @Max(Number.MAX_SAFE_INTEGER)
  perMinute: number;
  @IsInt()
  @Min(0)
  @Max(Number.MAX_SAFE_INTEGER)
  minWait: number;
  @IsInt()
  @Min(0)
  @Max(Number.MAX_SAFE_INTEGER)
  randomWait: number;

  constructor(that: IQueueThrottlerConfig) {
    this.perMinute = that.perMinute;
    this.minWait = that.minWait;
    this.randomWait = that.randomWait;
    checkValidateSync(this);
  }
}
