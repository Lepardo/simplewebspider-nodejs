import { config } from "./core/config";
import { logger } from "./core/logger";
import { processOn } from "./core/processOn";
import { SimpleBootstrapper } from "./service/bootstrap";
import { SimplerCrawler } from "./service/crawler";
import { HostThrottlerDatabase, LimitedMemoryDatabase } from "./service/database";
import { FilterInvalidUrlsExtrator, SimpleExtractor } from "./service/extractor";
import { PromiseQueue, QueueThrottlerQueue } from "./service/queue";

// Update loglevel after loading configuration
// updateLogger(config.log); // TypeError: logger_1.updateLogger is not a function
logger.level = config.log.level;

logger.debug(`Initiate process on listeners...`);
processOn();

logger.info(`Build ${JSON.stringify(config.build)}`);
logger.debug(`Configuration ${JSON.stringify(config)}`);

logger.info(`Wire services`);
const bootstrapper = new SimpleBootstrapper(config.bootstrap);
const limitedMemorydatabase = new LimitedMemoryDatabase(config.database);
const hostThrottlerDatabase = new HostThrottlerDatabase(limitedMemorydatabase, config.hostThrottler);
const simpleExtractor = new SimpleExtractor(config.extractor);
const filterInvalidUrlsExtrator = new FilterInvalidUrlsExtrator(simpleExtractor, config.filterInvalidUrl);
const queue = new PromiseQueue(config.queue);
const queueThrottlerQueue = new QueueThrottlerQueue(queue, config.queueThrottler);
const crawler = new SimplerCrawler(hostThrottlerDatabase, bootstrapper, filterInvalidUrlsExtrator, queueThrottlerQueue);

logger.info(`Starting crawler...`);
crawler.run();
