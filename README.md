

[![npm version](https://badge.fury.io/js/simplewebspider-nodejs.svg)](https://badge.fury.io/js/simplewebspider-nodejs) [![downloads per month](https://img.shields.io/npm/dm/simplewebspider-nodejs)](https://www.npmjs.com/package/simplewebspider-nodejs) [![official documentation](https://img.shields.io/badge/documentation-%23282ea9.svg)](https://gitlab.com/Lepardo/simplewebspider-nodejs)

[![pipeline status](https://gitlab.com/Lepardo/simplewebspider-nodejs/badges/master/pipeline.svg)](https://gitlab.com/Lepardo/simplewebspider-nodejs/-/pipelines) [![coverage report](https://gitlab.com/Lepardo/simplewebspider-nodejs/badges/master/coverage.svg)](https://gitlab.com/Lepardo/simplewebspider-nodejs/-/pipelines) [![Coverage Status](https://coveralls.io/repos/github/Barkole/simplewebspider-nodejs/badge.svg?branch=master)](https://coveralls.io/github/Barkole/simplewebspider-nodejs?branch=master)

[![License](https://img.shields.io/npm/l/simplewebspider-nodejs.svg)](https://gitlab.com/Lepardo/simplewebspider-nodejs/-/blob/master/LICENSE)

# simplewebspider-nodejs

## Description

Implementation of a simple web spider crawling the net never ending.

## Configuration

You can configure some behavior by setting environment variables or using the file `.env`.

The possible configuration is available in [config.ts](./src/core/config/config.ts).

## Available

* <https://gitlab.com/Lepardo/simplewebspider-nodejs>
* <https://yarnpkg.com/package/simplewebspider-nodejs>
* <https://www.npmjs.com/package/simplewebspider-nodejs>

## Contributing

See [CONTRIBUTING.md](./CONTRIBUTING.md)
