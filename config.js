/* eslint-disable prettier/prettier */
module.exports = {
    platform: `gitlab`,
    // endpoint: `https://gitlab.acme.com/api/v4/`,
    token: process.env.RENOVATE_TOKEN,

    repositories: [`Lepardo/simplewebspider-nodejs`],

    logLevel: `info`,

    requireConfig: true,
    onboarding: true,
    onboardingConfig: {
        extends: [`config:base`],
        prConcurrentLimit: 5,
    },

    enabledManagers: [`yarn`],
};
