import { randomInt } from "../../../src/core/utils";
import "../../test-utils/matchers/toBeInteger";
import "../../test-utils/matchers/toBeWithinRange";

describe(`randomInt`, () => {
  test(`result is in range`, () => {
    // Given
    const inputMin = -1;
    const inputMax = 1;

    // When
    const actual = randomInt(inputMin, inputMax);

    // Then
    expect(actual).toBeWithinRange(inputMin, inputMax);
  });

  test(`min and max are allowed for safe integers`, () => {
    // Given
    const inputMin = Number.MIN_SAFE_INTEGER;
    const inputMax = Number.MAX_SAFE_INTEGER;

    // When
    const actual = randomInt(inputMin, inputMax);

    // Then
    expect(actual).toBeWithinRange(inputMin, inputMax);
  });

  test(`result is integer`, () => {
    // Given
    const inputMin = Number.MIN_SAFE_INTEGER;
    const inputMax = Number.MAX_SAFE_INTEGER;

    // When
    const actual = randomInt(inputMin, inputMax);

    // Then
    expect(actual).toBeInteger();
  });

  test(`result is not NaN`, () => {
    // Given
    const inputMin = Number.MIN_SAFE_INTEGER;
    const inputMax = Number.MAX_SAFE_INTEGER;

    // When
    const actual = randomInt(inputMin, inputMax);

    // Then
    expect(actual).not.toBeNaN();
  });

  test(`min equals to max return min/max value`, () => {
    // Given
    const inputMin = -1;
    const inputMax = -1;

    // When
    const actual = randomInt(inputMin, inputMax);

    // Then
    expect(actual).toStrictEqual(inputMin);
    expect(actual).toStrictEqual(inputMax);
  });

  test(`min must not be greater than max`, () => {
    // Given
    const inputMin = 1;
    const inputMax = -1;

    // When
    expect(() => randomInt(inputMin, inputMax))
      //
      // Then
      .toThrowError(`Invalid parametes: min is greater than max`);
  });

  test(`min must not be less than min safe integer `, () => {
    // Given
    const inputMin = Number.MIN_SAFE_INTEGER - 1;
    const inputMax = -1;

    // When
    expect(() => randomInt(inputMin, inputMax))
      //
      // Then
      .toThrowError(`Invalid parametes: min is less than min safe integer`);
  });

  test(`max must not be greater than max safe integer `, () => {
    // Given
    const inputMin = -1;
    const inputMax = Number.MAX_SAFE_INTEGER + 1;

    // When
    expect(() => randomInt(inputMin, inputMax))
      //
      // Then
      .toThrowError(`Invalid parametes: max is greater than max safe integer`);
  });
});
