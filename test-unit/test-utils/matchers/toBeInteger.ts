/* eslint-disable @typescript-eslint/no-namespace */

// ensure this is parsed as a module.
// eslint-disable-next-line prettier/prettier
export { };

declare global {
  namespace jest {
    interface Matchers<R> {
      toBeInteger(): R;
    }
  }
}

expect.extend({
  toBeInteger(received) {
    const pass = Number.isInteger(received);
    if (pass) {
      return {
        message: (): string => `expected ${received} not to be an integer`,
        pass: true,
      };
    } else {
      return {
        message: (): string => `expected ${received} to be an integer`,
        pass: false,
      };
    }
  },
});
