#!/bin/bash
set -e # Exit on first error
set -E
set -o functrace
function handle_error() {
    local retval=$?
    local line=${last_lineno:-$1}
    # Sleep to allow others to print their errors first
    sleep 1
    echo "Failed at ${line}: ${BASH_COMMAND}"
    echo "Trace: " "$@"
    exit $retval
}
if ((${BASH_VERSION%%.*} <= 3)) || [[ ${BASH_VERSION%.*} == 4.0 ]]; then
    trap '[[ $FUNCNAME = handle_error ]] || { last_lineno=$real_lineno; real_lineno=$LINENO; }' DEBUG
fi
trap 'handle_error $LINENO ${BASH_LINENO[@]}' ERR


echo " ===> Create release ${npm_package_version}..."
echo " => Check for uncommitted changes..."
if [[ ! -z $(git status -s) ]]; then
    echo "ERROR: Git status is not clean!"
    echo "${GIT_STATUS}"
    exit 1
fi

echo " => check for being behind..."
git fetch --dry-run

echo " => check for being on master..."
if [[ -z $(git show-branch  --current | grep "[master]") ]]; then
    echo "ERROR: Not on master"
    git show-branch  --current
    exit 1
fi

echo " => Executing tests..."
yarn test

echo " => Create release version..."
yarn version

echo " => Create prepatch version without tag..."
export SKIP_ARTIFACTS=true
yarn version --prepatch --no-git-tag-version

echo "WARN: Binaries not checked in!"
ls -1 ./dist-binaries/sws-*
