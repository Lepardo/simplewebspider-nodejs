#!/bin/bash
set -e # Exit on first error
set -E
set -o functrace
function handle_error() {
    local retval=$?
    local line=${last_lineno:-$1}
    # Sleep to allow others to print their errors first
    sleep 1
    echo "Failed at ${line}: ${BASH_COMMAND}"
    echo "Trace: " "$@"
    exit $retval
}
if ((${BASH_VERSION%%.*} <= 3)) || [[ ${BASH_VERSION%.*} == 4.0 ]]; then
    trap '[[ $FUNCNAME = handle_error ]] || { last_lineno=$real_lineno; real_lineno=$LINENO; }' DEBUG
fi
trap 'handle_error $LINENO ${BASH_LINENO[@]}' ERR

echo " => Version ${npm_package_version}..."

if [[ "${SKIP_ARTIFACTS}" == "true" ]]; then
    echo "INFO: Skipping artifact creation"
    exit 0
fi

mkdir -p ./artifacts

echo " => Updating module dependency graph..."
yarn madge --no-spinner --ts-config tsconfig.json --extensions ts,tsx --image ./artifacts/dependencies.svg ./src

echo " => Creating release binaries..."
yarn build-pkg
# These binaries are to big and change to often
#cp ./dist-binaries/* ./artifacts/

echo " => Check git status ${npm_package_version}..."
if [[ ! -z $(git status -s) ]]; then
    echo "Uncommitted changes found..."
    echo " => Stage all changes..."
    git add -A -v
fi
