#!/bin/bash
set -e # Exit on first error
set -E
set -o functrace
function handle_error() {
    local retval=$?
    local line=${last_lineno:-$1}
    # Sleep to allow others to print their errors first
    sleep 1
    echo "Failed at ${line}: ${BASH_COMMAND}"
    echo "Trace: " "$@"
    exit $retval
}
if ((${BASH_VERSION%%.*} <= 3)) || [[ ${BASH_VERSION%.*} == 4.0 ]]; then
    trap '[[ $FUNCNAME = handle_error ]] || { last_lineno=$real_lineno; real_lineno=$LINENO; }' DEBUG
fi
trap 'handle_error $LINENO ${BASH_LINENO[@]}' ERR

# Must no clean up, as we use incremental compilation
#echo " => cleaning ./dist"
#rm -rf ./dist

readonly DST="./dist"
readonly FILTERS="./filters"
export BUILD_TIMESTAMP=`date -Is -u`
if [[ -z $(git status -s) ]]; then
    export BUILD_SHA=`git rev-parse HEAD`
else
    export BUILD_SHA=`git rev-parse HEAD`-DIRTY
fi

echo " => dist folder..."
mkdir -p ${DST}

echo " => Copy resources..."
cp -R ./resources/* ${DST}/

echo " => Process filters..."
for file in `find ${FILTERS} -type f`; do
    echo "- ${file}:"
    SRC_PATH=`dirname ${file}`
    REL_PATH=${SRC_PATH#${FILTERS}}
    if [[ ! "${REL_PATH}" == "" ]]; then
        echo "  Create folder: ${REL_PATH}"
        mkdir -p "${DST}/${REL_PATH}"
    fi
    echo "  Envsubst: ${file#${FILTERS}}"
    envsubst < "$file" > "${DST}/${file#${FILTERS}}"
done

echo " => tsc..."
yarn tsc --resolveJsonModule
