# Contribute

## Getting started

1. Install [VSCode](https://code.visualstudio.com/)
2. Install [Git Bash for Windows](https://gitforwindows.org/)
3. Clone project: `git clone git@gitlab.com:Lepardo/simplewebspider-nodejs.git`
4. Setup your git
   1. `git config user.name "foo"`
   2. `git config user.email "email@example.com"`
5. Install [node.js 12.x](https://nodejs.org/)
6. Install [yarn](https://yarnpkg.com/) - [Installation](https://classic.yarnpkg.com/en/docs/install/#windows-stable)
7. Install all dependencies: `yarn install`
8. Optional: Install [pkg](https://www.npmjs.com/package/pkg): `yarn global add pkg` - Required to enable `yarn ip build-pkg`
9. Optional: Enable [snyk](https://app.snyk.io) - Required to enable `yarn snyk-*`
   1.  Create an account at <https://app.snyk.io>
   2.  Authenticate `yarn snyk-auth`
10. Optional: Install [GraphViz](https://graphviz.org/download/) - required to enable `yarn madge --extensions ts,tsx --image dependencies.svg ./src`
11. Optional: Setup environment variable `RENOVATE_TOKEN` with GitLab access token 
12. Setup VSCode
    1.  Install recommended extensions

## Coding

### Overview

#### Module dependencies

![module dependencies](./artifacts/dependencies.svg "Logo Title Text 1")

#### Renovate

Run

```sh
yarn renovate
```

to create MRs to update dependencies.

See also <https://simplabs.com/blog/2019/04/24/dependency-updates-for-gitlab/>

### Known issues

#### gvpr - Segmentation fault (core dumped)

`gvpr` causes

```txt
$ gvpr
Segmentation fault (core dumped)
```

See also <https://stackoverflow.com/questions/62583745/docker-alpine-graphviz-gvpr-causes-segmentation-fault-core-dumped>

#### Pkg does not work in docker

[Bug: Pkg does not create correct exe in docker #930](https://github.com/vercel/pkg/issues/930)

#### Auto compile on save is not working

* [The terminal process terminated with exit code: 127 #100229](https://github.com/microsoft/vscode/issues/100229)
* [VSCode path generation failure in Run Build Task / tsc: build](https://stackoverflow.com/questions/49910024/vscode-path-generation-failure-in-run-build-task-tsc-build)
* [Paths separators in build config being escaped/stripped out prior to build command being run #35593](https://github.com/Microsoft/vscode/issues/35593)


#### Depcheck has false positives report

* [Bug: false unused reports with eslint #552](https://github.com/depcheck/depcheck/issues/552)
* [Bug: false positive "@types/jest" using jest and typescript #553](https://github.com/depcheck/depcheck/issues/553)

### TODOs

#### test other logger

* <https://github.com/cabinjs/cabin>

#### Add more security checks

* <https://github.com/lirantal/awesome-nodejs-security>
* <https://github.com/ajinabraham/nodejsscan>
* <https://github.com/AppThreat/sast-scan>
* <https://github.com/lirantal/awesome-nodejs-security>
* <https://github.com/lirantal/npq>

#### Implement Mimetype check

* <https://gitlab.com/Lepardo/SimpleWebSpider-Android/-/blob/master/app/src/main/java/com/android/webcrawler/bot/CrawlerImpl.java#L225>

#### Support more tags

* <https://gitlab.com/Lepardo/SimpleWebSpider-Android/-/blob/master/app/src/main/java/com/android/webcrawler/bot/extractor/html/stream/TagListenerImpl.java#L30>

#### Check zero values in configuration

Either must be forbidden
Or must be handled correctly (unlimitted)

##### Testing

* Unit testing
* ...
* <https://kentcdodds.com/blog/stop-mocking-fetch>
* <https://github.com/dubzzz/fast-check>

### Executing

* Normal and safe start:

   ```sh
   > yarn i start
   ```

* Quick start thanks to autocompile on save - if it would work

   ```sh
   > yarn start-quick
   ```

### Debugging

Launch `Run npm start`

### Managing dependencies

Only use

* `yarn add`
* `yarn remove`
* `yarn upgrade-interactive --latest`

### Evaluate new dependency

Please use and compare several 3rd party libraries by using

* <https://npmcompare.com/>
* <https://www.npmtrends.com/>
* <https://bundlephobia.com/>
* <https://packagephobia.com/>
* <https://openbase.io/>

For upgrade you can also dig into changes by using

* <https://diff.intrinsic.com/>

### How to release

1. Being on master
2. Everything commited
3. Not behind
4. Execute

   ```txt
   > yarn release
   ```

## Related documentation

* [TypeScript Tutorial](https://www.typescriptlang.org/docs/tutorial.html)
  * [TypeScript-Node-Starter](https://github.com/microsoft/TypeScript-Node-Starter/blob/master/README.md)
* [yarn - Creating a new project](https://classic.yarnpkg.com/en/docs/creating-a-project/)
* Check for ECMAScript support: [node.green](https://node.green/)
  * See also [ECMAScript 2015 (ES6) and beyond](https://nodejs.org/en/docs/es6/)
